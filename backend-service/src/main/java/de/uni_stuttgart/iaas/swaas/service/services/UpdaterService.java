package de.uni_stuttgart.iaas.swaas.service.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_stuttgart.iaas.swaas.service.model.Sensor;
import de.uni_stuttgart.iaas.swaas.service.model.dto.SensorMessage;
import lombok.extern.java.Log;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
@Log
@Service
public class UpdaterService {

    @Value("${de.uni_stuttgart.iaas.swaas.topic}")
    private String topic;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MqttService mqttService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private SensorService sensorService;


    @PostConstruct
    public void init(){
        try {
            mqttService.getBroker().subscribe(topic, (topic, msg) -> {
                log.info("RECV MSG");
                SensorMessage sensorMessage = mapper.readValue(msg.getPayload(), SensorMessage.class);
                Sensor sensor = sensorService.getSensor(sensorMessage.getInstanceId());
                if(sensor == null || sensor.getStorageUnit() == null) {
                    log.warning("No Sensor discover yet that has Id: " + sensorMessage.getInstanceId());
                    return;
                }
                sensorMessage.setStorageUnitId(sensor.getStorageUnit());
                msg.setPayload(mapper.writeValueAsBytes(sensorMessage));
                simpMessagingTemplate.convertAndSend("/updates", msg);
            });
        } catch (MqttException e) {
            if(!mqttService.getBroker().isConnected()) {
                mqttService.init();
            }
        }
    }


}
