import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {SensorsService} from '../../services/sensors.service';
import {StorageUnit} from '../../model/storage-unit';
import {SensorPoint} from '../../model/sensor-point';
import {MessageService} from '../../services/message.service';
import {ProductsService} from '../../services/products.service';
import {ActuatorService} from '../../services/actuator.service';


@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {



  constructor(private sensorsService: SensorsService,
              private messageService: MessageService,
              private productsService: ProductsService,
              private regulatorService: ActuatorService,
              private ref: ChangeDetectorRef) {  }

  temperatureData: any[];
  humidtyData: any[];
  luminanceData: any[];
  powerData: any[];

  temperature: number;
  humidity: number;
  luminance: number;
  power = 0;
  temperatureUnit: string;
  humidityUnit: string;
  luminanceUnit: string;
  powerUnit = 'W';
  cooling = 0;

  timestamp: Date;

  @Input()
  storageUnit: StorageUnit;

  unit: string;
  sensorIds: string[];
  arriving: any;
  stored: any;


  switch = true;



  ngOnInit(): void {
    console.log(this.storageUnit.id);
    const ids = Object.assign([], this.storageUnit.sensorIds);
    let sensorId = ids.pop();
    this.sensorsService.getSensorSeconds(sensorId, 1000).subscribe(result => {
      this.temperatureData = this.createChartData(result);
    });

    sensorId = ids.pop();
    this.sensorsService.getSensorSeconds(sensorId, 1000).subscribe(result => {
      this.powerData = this.createChartData(result);
    });
    ids.pop(); // Skip plug state
    sensorId = ids.pop();
    this.sensorsService.getSensorSeconds(sensorId, 1000).subscribe(result => {
      this.luminanceData = this.createChartData(result);
    });

    sensorId = ids.pop();
    this.sensorsService.getSensorSeconds(sensorId, 1000).subscribe(result => {
      this.humidtyData = this.createChartData(result);
    });

    this.messageService.register(this.storageUnit.id).subscribe(msg => {

        if (this.storageUnit.id === msg.storageUnitId) {
          this.timestamp = new Date(msg.timestamp);
          switch (msg.sensorType.substr(msg.sensorType.lastIndexOf('/') + 1, msg.sensorType.length)) {
            case 'temperature':
              this.temperature = msg.value.value;
              this.temperatureUnit = msg.value.unit;
              // console.log('Set Temp in', this.storageUnit.id.toString());
              break;
            case 'humidity':
              this.humidity = msg.value.value;
              this.humidityUnit = msg.value.unit;
              // console.log('Set Hum in', this.storageUnit.id.toString());
              break;
            case 'luminance':
              this.luminance = msg.value.value;
              this.luminanceUnit = msg.value.unit;
              // console.log('Set Lum in', this.storageUnit.id.toString());
              break;
            case 'power_consumption':
              this.power = msg.value.value;
              this.powerUnit = msg.value.unit;
              // console.log('Set power in', this.storageUnit.id.toString());
              break;
            case 'plug_state':
              this.cooling = msg.value.value;
              // console.log('Set Plug in', this.storageUnit.id.toString());
              break;
            default:
              break;
          }
          this.ref.detectChanges();
        } else {
          // console.log('Im in', this.storageUnit.id.toString(), 'but message was for', msg.storageUnitId);
        }

    });

    this.getProducts();
    setInterval(() => {
      this.getProducts();
    }, 2000);
  }

  private getProducts() {
    this.productsService.getProducts(this.storageUnit.id).subscribe(result => {
      // console.log('Got it');
      const arrv = [];
      const prods = [];
      result.forEach(product => {
        // console.log('PROD:', product);
        if (this.productsService.isProductToBeStored(product)) {
          arrv.push(product);
        } else if (this.productsService.isProductStored(product)) {
          prods.push(product);
        }
      });
      this.arriving = arrv;
      this.stored = prods;
    });
  }

  private createChartData(result: SensorPoint[]) {
    const data = [];
    const seriesAr = [];
    const seriesMax = [];
    const seriesMin = [];
    let seriesName = '';
    result.forEach((point) => {
      seriesName = point.measure.substr(point.measure.lastIndexOf('/') + 1, point.measure.length);
      const item = {name: point.timestamp, value: point.value};
      seriesAr.push(item);
      if (seriesName !== 'power_consumption') {
        seriesMax.push({name: point.timestamp, value: this.getMaxValue(seriesName)});
        seriesMin.push({name: point.timestamp, value: this.getMinValue(seriesName)});
      }
    });
    data.push({name: seriesName, series: seriesAr});
    if (seriesMin.length > 0 && seriesMax.length > 0) {
      data.push({name: 'Maximal', series: seriesMax});
      data.push({name: 'Minimal', series: seriesMin});
    }
    return data;
  }

  private getMinValue(type: string) {
    let minValue = -123;
    switch (type) {
      case 'temperature':
        minValue = this.storageUnit.storageConditionsBoundaries.temperatureMin;
        break;
      case 'humidity':
        minValue = this.storageUnit.storageConditionsBoundaries.humidityMin;
        break;
      case 'luminance':
        minValue = this.storageUnit.storageConditionsBoundaries.lightMin;
        break;
      default:
        break;
    }
    return minValue;
  }

  private getMaxValue(type: string) {
    let maxValue = -123;
    switch (type) {
      case 'temperature':
        maxValue = this.storageUnit.storageConditionsBoundaries.temperatureMax;
        break;
      case 'humidity':
        maxValue = this.storageUnit.storageConditionsBoundaries.humidityMax;
        break;
      case 'luminance':
        maxValue = this.storageUnit.storageConditionsBoundaries.lightMax;
        break;
      default:
        break;
    }
    return maxValue;
  }


  public switchLight() {
    if (this.switch) {
      this.regulatorService.setLightState(this.storageUnit.id, 'ON').subscribe(result => {
        this.switch = false;
      });
    } else {
      this.regulatorService.setLightState(this.storageUnit.id, 'OFF').subscribe(result => {
        this.switch = true;
      });
    }
  }

  public turnOffCooling() {
    this.regulatorService.setCoolingState(this.storageUnit.id , 'OFF').subscribe();
  }

  public turnOnCooling() {
    this.regulatorService.setCoolingState( this.storageUnit.id, 'ON').subscribe();
  }



}
