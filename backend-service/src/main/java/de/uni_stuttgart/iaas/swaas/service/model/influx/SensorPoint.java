package de.uni_stuttgart.iaas.swaas.service.model.influx;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SensorPoint {

    private String measure;
    private String instanceId;
    private String unit;
    private double value;
    private String timestamp;


}
