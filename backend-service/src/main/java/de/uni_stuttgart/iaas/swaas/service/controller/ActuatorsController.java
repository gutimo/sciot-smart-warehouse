package de.uni_stuttgart.iaas.swaas.service.controller;

import de.uni_stuttgart.iaas.swaas.service.model.ProductItem;
import de.uni_stuttgart.iaas.swaas.service.services.RegulatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActuatorsController {
    private static final String context = "/actuators";

    @Autowired
    private RegulatorService regulatorService;

    @GetMapping(context + "/light/{unitId}/{state}")
    public ResponseEntity<Boolean> setLightState(@PathVariable String unitId, @PathVariable String state){
        return new ResponseEntity<>(regulatorService.setLightState(unitId, RegulatorService.ActuatorState.valueOf(state)), HttpStatus.OK);
    }

    @GetMapping(context + "/cooling/{unitId}/{state}")
    public ResponseEntity<Boolean> setCoolingState(@PathVariable String unitId, @PathVariable String state){
        return new ResponseEntity<>(regulatorService.setCoolingState(unitId, RegulatorService.ActuatorState.valueOf(state)), HttpStatus.OK);
    }
}
