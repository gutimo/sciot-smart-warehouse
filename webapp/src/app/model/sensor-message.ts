import {SensorValue} from './sensor-value';

export class SensorMessage {
  instanceId: string;
​
  sensorType: string;
​
  timestamp: number;
​
  value: SensorValue;

  storageUnitId: number;

}
