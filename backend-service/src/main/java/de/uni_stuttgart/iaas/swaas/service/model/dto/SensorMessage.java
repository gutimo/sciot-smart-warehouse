package de.uni_stuttgart.iaas.swaas.service.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SensorMessage {

    private String instanceId;
    private String sensorType;
    private long timestamp;
    private Value value;
    private String storageUnitId;


}
