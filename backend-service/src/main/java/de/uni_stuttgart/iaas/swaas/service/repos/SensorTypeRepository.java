package de.uni_stuttgart.iaas.swaas.service.repos;

import de.uni_stuttgart.iaas.swaas.service.model.SensorType;
import org.springframework.data.repository.CrudRepository;

public interface SensorTypeRepository extends CrudRepository<SensorType, String> {



}
