package de.uni_stuttgart.iaas.swaas.config;

import java.io.*;
import java.util.Properties;

public class Configuration {

    private static final String DATABASE_USER_KEY = "de.uni_stuttgart.iaas.swaas.database.user";
    private static final String DATABASE_PW_KEY = "de.uni_stuttgart.iaas.swaas.database.pw";
    private static final String DATABASE_URL_KEY = "de.uni_stuttgart.iaas.swaas.database.url";
    private static final String BROKER_USER_KEY = "de.uni_stuttgart.iaas.swaas.broker.user";
    private static final String BROKER_PW_KEY = "de.uni_stuttgart.iaas.swaas.broker.pw";
    private static final String BROKER_URL_KEY = "de.uni_stuttgart.iaas.swaas.broker.url";
    private static final String TOPIC_KEY = "de.uni_stuttgart.iaas.swaas.topic";

    private static Properties config;
    private static  final InputStream file = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");



    static  {
        config = new Properties();
        try {
            config.load(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getDatabaseUser() {
        return config.getProperty(DATABASE_USER_KEY);
    }

    public static String getDatabasePassword() {
        return config.getProperty(DATABASE_PW_KEY);
    }

    public static String getDatabaseUrl() {
        return config.getProperty(DATABASE_URL_KEY);
    }

    public static String getBrokerUser() {
        return config.getProperty(BROKER_USER_KEY);
    }

    public static String getBrokerPassword() {
        return config.getProperty(BROKER_PW_KEY);
    }

    public static String getBrokerUrl() {
        return config.getProperty(BROKER_URL_KEY);
    }

    public static String getTopicUrl() {
        return config.getProperty(TOPIC_KEY);
    }

}
