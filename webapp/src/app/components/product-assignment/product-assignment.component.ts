import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../model/product';
import {ProductsService} from '../../services/products.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatTabChangeEvent} from '@angular/material/tabs';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {DialogComponent} from '../dialog/dialog.component';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-product-assignment',
  templateUrl: './product-assignment.component.html',
  styleUrls: ['./product-assignment.component.scss']
})
export class ProductAssignmentComponent implements OnInit {
  products: BehaviorSubject<Product[]> = this.productsService.products;
  displayedColumns: string[] = ['id', 'name', 'customer', 'quantity', 'storedFrom', 'storedUntil', 'storageUnitId'];

  incomingFilter = (product: Product) => this.productsService.isProductToBeStored(product);
  storedFilter = (product: Product) => this.productsService.isProductStored(product);
  finishedFilter = (product: Product) => this.productsService.isProductFinished(product);

  constructor(public productsService: ProductsService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.productsService.getAllProducts();
    setInterval(() => {
      this.productsService.getAllProducts();
    }, 2000);
  }

  // onRowClick(product: Product): void {
  //   if (this.productsService.isProductToBeStored(product)) {
  //     this.productsService.getStorageAssignment(product.id).subscribe((storageUnitId: string) => {
  //       const dialogRef = this.openDialog(storageUnitId);
  //       dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
  //         if (dialogResult) {
  //           this.productsService.storeProduct(product.id, storageUnitId);
  //         }
  //       });
  //     });
  //   } else if (this.productsService.isProductStored(product)) {
  //     this.productsService.unStoreProduct(product.id);
  //   }
  // }
  //
  // openDialog(productId: string) {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.data = {storageUnitId: productId};
  //   return this.dialog.open(DialogComponent, dialogConfig);
  // }
  //
  // applyFilter(event: MatTabChangeEvent): void {
  //   this.products.filter = event.tab.textLabel;
  // }

}
