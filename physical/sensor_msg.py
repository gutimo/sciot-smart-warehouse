
import time
import json

class SensorMSG:

    sensor_value_dict = {"instanceId": "",
    "sensorType": "",
    "value": {
    "unit": None,
    "value": None
    },
    "timestamp": 0
    }

    def __init__(self, instanceId, sensorType, unit, value):
        self.instanceId = instanceId
        self.sensorType = sensorType
        self.unit = unit
        self.value = value

    def to_json(self):
        temp_msg = self.sensor_value_dict.copy()
        temp_msg["instanceId"] = self.instanceId
        temp_msg["sensorType"] = "de.uni-stuttgart.iaas.swaas/" + self.sensorType
        temp_msg["value"]["unit"] = self.unit
        temp_msg["value"]["value"] = self.value
        temp_msg["timestamp"] = int(round(time.time() * 1000))
        return json.dumps(temp_msg)