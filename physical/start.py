import paho.mqtt.client as mqtt
import json
import temp_hum_sense as ths
import luminance_sense
import sensor_msg
import zigbee_consumer
import zigbee_switch_act
import light_act
import time
import config

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT broker with result code "+str(rc))


print("===STARTUP===")
client = mqtt.Client()
client.on_connect = on_connect
print("Trying to connect to broker")
# TODO Extract credentials and host config
client.username_pw_set(config.broker_user, password=config.broker_pw)
client.connect(config.broker_url, 1883)
client.message_callback_add('zigbee2mqtt/0x04cf8cdf3c7b23a0', zigbee_consumer.on_message)
client.message_callback_add('cooling', zigbee_switch_act.on_message)
client.message_callback_add('light', light_act.on_message)
client.loop_start()
th_sensor = ths.DHT()
lux_sensor = luminance_sense.LightSensor()

topic = config.topic
print("===STARTING===")
while True:
    temperatur_msg = sensor_msg.SensorMSG(th_sensor.temp_instance_id, th_sensor.temp_sensor_type, th_sensor.temp_unit, th_sensor.get_temp())
    client.publish(topic, temperatur_msg.to_json())
    time.sleep(1)
    humidity_msg = sensor_msg.SensorMSG(th_sensor.hum_instance_id, th_sensor.hum_sensor_type, th_sensor.hum_unit, th_sensor.get_hum())
    client.publish(topic, humidity_msg.to_json())
    time.sleep(1)
    lux_msg = sensor_msg.SensorMSG(lux_sensor.instanceId, lux_sensor.sensorType, lux_sensor.unit, lux_sensor.getLux())
    client.publish(topic, lux_msg.to_json())
    time.sleep(5)

