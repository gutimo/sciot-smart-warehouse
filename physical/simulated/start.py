import paho.mqtt.client as mqtt
import json
import temp_hum_sense as ths
import luminance_sense
import sensor_msg
import simulated_storage_unit
import time
from threading import Thread
import config

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT broker with result code "+str(rc))


print("===STARTUP SIMULATED===")
client = mqtt.Client()
client.on_connect = on_connect
print("Trying to connect to broker")
# TODO Extract credentials and host config
client.username_pw_set(config.broker_user, password="config.broker_pw")
client.connect(config.broker_url, 1883)
client.subscribe("light_state/#")
client.subscribe("cooling_state/#")

storage_unit_1002 = simulated_storage_unit.SimulatedStorageUnit("1002", client, 20.0, 85.0, 0.0, 10)
storage_unit_1003 = simulated_storage_unit.SimulatedStorageUnit("1003", client, -13.5, 50.0, 1.0, 50)

client.loop_start()

Thread(target = storage_unit_1002.startMeasure, args=[]).start()
Thread(target = storage_unit_1003.startMeasure, args=[]).start()
