import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActuatorService {

  url = 'api/actuators';

  constructor(private http: HttpClient) { }

  public setLightState(unitId: number, state: string): Observable<boolean> {
    return this.http.get<boolean>(this.url + '/light/' + unitId + '/' + state);
  }

  public setCoolingState(unitId: number, state: string): Observable<boolean> {
    return this.http.get<boolean>(this.url + '/cooling/' + unitId + '/' + state);
  }

}
