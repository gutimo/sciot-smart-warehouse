import paho.mqtt.client as mqtt
import json
import temp_hum_sense as ths
import luminance_sense
import sensor_msg
import time


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT broker with result code "+str(rc))

def start():
    print("===STARTUP===")
    client = mqtt.Client()
    client.on_connect = on_connect

    print("Trying to connect to broker")
    # TODO Extract credentials and host config
    client.username_pw_set("grovepi", password="grovepi")
    client.connect("gutierrez.one", 1883)
    client.loop_start()

    th_sensor = ths.DHT()
    lux_sensor = luminance_sense.LightSensor()

    print("===STARTING===")
    while True:
        temperatur_msg = sensor_msg.SensorMSG(th_sensor.temp_instance_id, th_sensor.temp_sensor_type, th_sensor.temp_unit, th_sensor.get_temp())
        client.publish("grovepi", temperatur_msg.to_json())
        time.sleep(1)
        humidity_msg = sensor_msg.SensorMSG(th_sensor.hum_instance_id, th_sensor.hum_sensor_type, th_sensor.hum_unit, th_sensor.get_hum())
        client.publish("grovepi", humidity_msg.to_json())
        time.sleep(1)
        lux_msg = sensor_msg.SensorMSG(lux_sensor.instanceId, lux_sensor.sensorType, lux_sensor.unit, lux_sensor.getLux())
        client.publish("grovepi", lux_msg.to_json())
        time.sleep(5)

start();
