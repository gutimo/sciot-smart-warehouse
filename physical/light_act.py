import paho.mqtt.client as mqtt
import sensor_msg
import json
import grovepi


def on_message(client, userdata, msg):
    # The callback for when a PUBLISH message is received from the server.
    message = msg.payload.decode('cp1252')
    if(message == 'ON'):
        grovepi.digitalWrite(2,1)
    else:
        grovepi.digitalWrite(2,0)
