package de.uni_stuttgart.iaas.swaas.db;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.uni_stuttgart.iaas.swaas.mqtt.model.SensorMessage;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;

import java.util.concurrent.TimeUnit;

import static de.uni_stuttgart.iaas.swaas.config.Configuration.*;

public class InfluxDBAdapter {
    // TODO Externalize
    final static String serverURL = getDatabaseUrl(), username = getDatabaseUser(), password = getDatabasePassword();
    final static InfluxDB influxDB = InfluxDBFactory.connect(serverURL, username, password);
    final static String databaseName = "sensors";

    private static InfluxDBAdapter instance;

    static {
        instance = new InfluxDBAdapter();
    }

    private InfluxDBAdapter() {
        // SINGLETON
    }


    public static InfluxDBAdapter getInstance() {
        return instance;
    }

    public void insertPoint(String sensorMessage) {
        SensorMessage msg = fromJson(sensorMessage);
        if (msg != null) {
            influxDB.query(new Query("CREATE DATABASE " + databaseName));
            influxDB.setDatabase(databaseName);

            // Write points to InfluxDB.
            influxDB.write(Point.measurement(msg.getSensorType())
                    .time(msg.getTimestamp(), TimeUnit.MILLISECONDS)
                    .tag("instanceId", msg.getInstanceId())
                    .tag("unit", msg.getValue().getUnit())
                    .addField("value", msg.getValue().getValue())
                    .build());
        }
    }

    private SensorMessage fromJson(String sensorMessage) {
        SensorMessage msg;
        try {
            msg = new ObjectMapper().readValue(sensorMessage, SensorMessage.class);
        } catch (JsonProcessingException e) {
            msg = null;
            e.printStackTrace();
        }
        return msg;
    }


}
