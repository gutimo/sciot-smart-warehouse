package de.uni_stuttgart.iaas.swaas.service.repos;

import de.uni_stuttgart.iaas.swaas.service.model.Sensor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SensorRepository extends CrudRepository<Sensor, String> {

    List<Sensor> findByStorageUnit(String storageUnitId);
}
