package de.uni_stuttgart.iaas.swaas.service.controller;


import de.uni_stuttgart.iaas.swaas.service.model.ProductItem;
import de.uni_stuttgart.iaas.swaas.service.services.PlannerService;
import de.uni_stuttgart.iaas.swaas.service.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductsController {

    private static final String context = "/products";

    @Autowired
    private ProductsService productsService;

    @Autowired
    private PlannerService plannerService;

    @GetMapping(context + "/forUnit/{unitId}")
    public ResponseEntity<Iterable<ProductItem>> getProductsForUnit(@PathVariable String unitId) {
        return new ResponseEntity<>(productsService.getProductsForUnit(unitId), HttpStatus.OK);
    }

    @GetMapping(context + "/all")
    public ResponseEntity<Iterable<ProductItem>> getAllProducts() {
        return new ResponseEntity<>(productsService.getAllProducts(), HttpStatus.OK);
    }

    @GetMapping(context + "/storageAssignment")
    public ResponseEntity<Iterable<ProductItem>> updateStorageAssignment() {
        return new ResponseEntity<>(plannerService.updateStorageAssignment(), HttpStatus.OK);
    }
}
