import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../model/product';
import {ProductsService} from '../services/products.service';
import {MatDialog} from '@angular/material/dialog';
import {BehaviorSubject} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
})
export class ProductTableComponent implements OnInit {

  @Input()
  inputProducts: BehaviorSubject<Product[]>;
  products: Product[] = [];

  @Input()
  displayedColumns: string[];
  @Input()
  filter: (product: Product) => boolean;

  constructor(public productsService: ProductsService, private dialog: MatDialog) { }

  ngOnInit(): void {
    // TODO: frequently update to ensure if time passes and products' states change
    this.inputProducts.pipe(map(products => products.filter(product => this.filter(product)))).subscribe(filteredProducts => {
      this.products = filteredProducts;
    });
  }

  // onRowClick(product: Product): void {
  //   if (this.productsService.isProductToBeStored(product)) {
  //     this.productsService.getStorageAssignment(product.id).subscribe((storageUnitId: string) => {
  //       const dialogRef = this.openDialog(storageUnitId);
  //       dialogRef.afterClosed().subscribe((dialogResult: boolean) => {
  //         if (dialogResult) {
  //           this.productsService.storeProduct(product.id, storageUnitId);
  //         }
  //       });
  //     });
  //   } else if (this.productsService.isProductStored(product)) {
  //     this.productsService.unStoreProduct(product.id);
  //   }
  // }

  // openDialog(productId: string) {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   dialogConfig.data = {storageUnitId: productId};
  //   return this.dialog.open(DialogComponent, dialogConfig);
  // }

}
