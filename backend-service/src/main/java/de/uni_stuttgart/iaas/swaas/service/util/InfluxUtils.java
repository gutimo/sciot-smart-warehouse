package de.uni_stuttgart.iaas.swaas.service.util;

import de.uni_stuttgart.iaas.swaas.service.model.Action;
import de.uni_stuttgart.iaas.swaas.service.model.influx.SensorPoint;
import lombok.extern.java.Log;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.QueryResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Log
@Component
public class InfluxUtils {



    @Value("${de.uni_stuttgart.iaas.swaas.database.url}")
    private String databaseURL;
    @Value("${de.uni_stuttgart.iaas.swaas.database.user}")
    private String userName;
    @Value("${de.uni_stuttgart.iaas.swaas.database.pw}")
    private String password;



    private static InfluxDB influxDB;

    @PostConstruct
    public void init() {
        influxDB = InfluxDBFactory.connect(databaseURL, userName, password);
    }


    public static InfluxDB getSensorDatabase() {
        return influxDB;
    }

    public static List<SensorPoint> parseSensorSeries(List<QueryResult.Series> seriesList) {
        List<SensorPoint> sensorPoints = new ArrayList<>();
        if(seriesList != null) {
            seriesList.forEach(series -> sensorPoints.addAll(parseSeries(series)));
        } else {
            log.warning("No Data in InfluxDB");
        }
        return sensorPoints;
    }

    public static List<SensorPoint> parseSeries(QueryResult.Series series) {
        List<SensorPoint> points = new ArrayList<>();

        series.getValues().forEach(entry -> {
            SensorPoint point = new SensorPoint();
            point.setMeasure(series.getName());
            String time = (String) entry.get(0);
            point.setTimestamp(time);
            if(entry.get(1) != null) {
                point.setInstanceId((String) entry.get(1));
                point.setUnit((String) entry.get(2));
                point.setValue((Double) entry.get(3));
                points.add(point);
            }
        });
        return points;
    }

    public static boolean writeActionLog(Action action) {
        influxDB.createDatabase("actions");

        Point point = Point.measurement(action.getActuatorType())
                .time(action.getTimestamp().getTime(), TimeUnit.MILLISECONDS)
                .addField("state", action.getState())
                .build();

        influxDB.write(point);
        influxDB.flush();
        return true;
    }

}
