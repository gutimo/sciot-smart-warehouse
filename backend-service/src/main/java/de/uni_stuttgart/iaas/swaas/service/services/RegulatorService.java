package de.uni_stuttgart.iaas.swaas.service.services;

import de.uni_stuttgart.iaas.swaas.service.model.Action;
import de.uni_stuttgart.iaas.swaas.service.model.Sensor;
import de.uni_stuttgart.iaas.swaas.service.model.StorageConditionsBoundaries;
import de.uni_stuttgart.iaas.swaas.service.model.StorageUnit;
import de.uni_stuttgart.iaas.swaas.service.model.influx.SensorPoint;
import lombok.extern.java.Log;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static de.uni_stuttgart.iaas.swaas.service.util.InfluxUtils.writeActionLog;

@Log
@Service
public class RegulatorService {

    public enum ActuatorState {
        ON, OFF
    }

    @Autowired
    private MqttService mqttService;

    @Autowired
    StorageUnitsService storageUnitsService;

    @Autowired
    SensorService sensorService;

    @Autowired
    UpdaterService updaterService;

    // TODO: Support multiple storageUnits, catch case when no sensor data is in influxdb
    @Scheduled(fixedDelay = 15000)
    public void regulateStorageConditions() {
        List<StorageUnit> storageUnits = storageUnitsService.getAllStorageUnits();
        for (StorageUnit storageUnit: storageUnits) {
            StorageConditionsBoundaries storageConditionsBoundaries = storageUnit.getStorageConditionsBoundaries();
            double targetHumidity = (storageConditionsBoundaries.getHumidityMin() + storageConditionsBoundaries.getHumidityMax()) / 2.0;
            double targetTemperature = (storageConditionsBoundaries.getTemperatureMin() + storageConditionsBoundaries.getTemperatureMax()) / 2.0;
            double targetLuminance = storageConditionsBoundaries.getLightMin();

            List<Sensor> sensors = sensorService.getSensorsByStorageUnitId(storageUnit.getId());
            List<SensorPoint> sensorPoints;
            for (Sensor sensor : sensors) {
                switch (sensor.getSensorType()) {
                    case "de.uni-stuttgart.iaas.swaas/temperature":
                        sensorPoints = sensorService.getSensorDataSeconds(sensor.getInstanceId(), 10);
                        double meanTemperature = calculateMean(sensorPoints);
                        regulateTemperature(storageUnit.getId(), targetTemperature, meanTemperature);
                        break;
                    case "de.uni-stuttgart.iaas.swaas/humidity":
                        sensorPoints = sensorService.getSensorDataSeconds(sensor.getInstanceId(), 10);
                        double meanHumidity = calculateMean(sensorPoints);
                        checkHumidity(targetHumidity, meanHumidity);
                        break;
                    case "de.uni-stuttgart.iaas.swaas/luminance":
                        sensorPoints = sensorService.getSensorDataSeconds(sensor.getInstanceId(), 10);
                        double meanLuminance = calculateMean(sensorPoints);
                        regulateLuminance(storageUnit.getId(), targetLuminance, meanLuminance);
                    default:
                        break;
                }
            }
        }
    }

    private double calculateMean(List<SensorPoint> sensorPoints) {
        double mean = 0;
        for (SensorPoint sensorPoint : sensorPoints) {
            mean += sensorPoint.getValue();
        }
        return mean / sensorPoints.size();
    }

    private void regulateTemperature(String unitId, double targetTemperature, double temperature) {
        double temperatureTolerance = 1.0;
        if (temperature >= targetTemperature + temperatureTolerance) {
            setCoolingState(unitId, ActuatorState.ON);
        } else if (temperature < targetTemperature - temperatureTolerance) {
            setCoolingState(unitId, ActuatorState.OFF);
        }
    }

    private void regulateLuminance(String unitId, double targetLuminance, double luminance) {
        double luminanceTolerance = 50.0;
        if (luminance >= targetLuminance + luminanceTolerance) {
            setLightState(unitId, ActuatorState.OFF);
        } else if (luminance < targetLuminance - luminanceTolerance) {
            setLightState(unitId, ActuatorState.ON);
        }
    }

    private void checkHumidity(double targetHumidity, double humidity) {
        double humidityTolerance = 10.0;
        if (humidity > targetHumidity + humidityTolerance || humidity < targetHumidity - humidityTolerance) {
            log.warning("Humidity out of boundary!");
        }
    }

    public boolean setLightState(String unitId, ActuatorState actuatorState) {
        try {
            MqttMessage mqttMessage = new MqttMessage();
            mqttMessage.setPayload(actuatorState.toString().getBytes());
            mqttService.getBroker().publish("light_state/" + unitId, mqttMessage);
            log.info("LightState was set: " + mqttMessage);
        } catch (MqttException e) {
            if(!mqttService.getBroker().isConnected()){
                mqttService.init();
                updaterService.init();
            }
            return false;
        }
        return true;
    }

    public boolean setCoolingState(String unitId, ActuatorState actuatorState) {
        try {
            MqttMessage mqttMessage = new MqttMessage();
            mqttMessage.setPayload(actuatorState.toString().getBytes());
            mqttService.getBroker().publish("cooling_state/" + unitId, mqttMessage);
        } catch (MqttException e) {
            if(!mqttService.getBroker().isConnected()){
                mqttService.init();
                updaterService.init();
            }
            return false;
        }
        return true;
    }

    public boolean execAction(String unitId, Action action) {
        if(action.getActuatorType().endsWith("cooling")) {
            setCoolingState(unitId, ActuatorState.valueOf(action.getState()));
        } else if(action.getActuatorType().endsWith("light")) {
            setLightState(unitId, ActuatorState.valueOf(action.getState()));
        }
        return writeActionLog(action);
    }

    public boolean execActions(String unitId, List<Action> actions) {
        AtomicBoolean success = new AtomicBoolean(true);
        actions.forEach(action -> success.set(success.get() && execAction(unitId, action)));
        return success.get();
    }
}
