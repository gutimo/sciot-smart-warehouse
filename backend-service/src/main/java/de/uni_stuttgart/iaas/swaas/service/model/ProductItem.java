package de.uni_stuttgart.iaas.swaas.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductItem {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    private String customer;
    private String storageUnitId;
    private long storedFrom;
    private long storedUntil;
    private double quantity;
    private String unit;

    private double humidityMin;
    private double temperatureMin;
    private double lightMin;
    private double humidityMax;
    private double temperatureMax;
    private double lightMax;

}
