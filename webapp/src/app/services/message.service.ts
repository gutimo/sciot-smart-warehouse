import {Injectable, OnInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import {SensorMessage} from '../model/sensor-message';
import {StorageUnitsService} from './storage-units.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService{

  subject: Map<number, BehaviorSubject<SensorMessage>>;


  constructor(private storageUnitsService: StorageUnitsService) {
    this.initializeWebSocketConnection();
  }

  stompClient;
  queue: SensorMessage[] = [];


  register(unitId: number) {
    let subj = this.subject.get(unitId);
    if (!subj) {
      subj = new BehaviorSubject<SensorMessage>(new SensorMessage());
      this.subject.set(unitId, subj);
    }
    return subj ;
  }

  initializeWebSocketConnection() {
    this.subject = new Map();
    const serverUrl = 'api/socket';
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.debug = null;
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/updates', (message) => {
        if (message.body) {
          const content = JSON.parse(message.body);
          that.pushUpdate(JSON.parse(atob(content.payload)));
        }
      });
    });

    setInterval(() => {
      if (this.queue.length > 0) {
        while (this.queue.length > 0) {
          const msg = this.queue.pop();
          if(this.subject.get(msg.storageUnitId)) {
            this.subject.get(msg.storageUnitId).next(msg);
          } else {
            this.subject.set(msg.storageUnitId, new BehaviorSubject(msg));
          }
        }
      }
    }, 1000);
  }

  pushUpdate(updateEvent) {
    this.queue.push(updateEvent);
  }

}
