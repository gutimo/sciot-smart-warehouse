package de.uni_stuttgart.iaas.swaas.service.repos;

import de.uni_stuttgart.iaas.swaas.service.model.ProductItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductItemRepository extends CrudRepository<ProductItem, Long > {

    List<ProductItem> findAllByStorageUnitId(String storageUnitId);

    List<ProductItem> findAll();

    List<ProductItem> findAllByStoredFromAfter(long date);

    List<ProductItem> findAllByStoredFromBeforeAndStoredUntilAfter(long date01, long date02);

    List<ProductItem> findAllByStorageUnitIdAndStoredFromBeforeAndStoredUntilAfter(String storageUnitId, long date01, long date02);

    ProductItem findById(long id);

    ProductItem save(ProductItem productItem);

    void deleteById(long id);

}
