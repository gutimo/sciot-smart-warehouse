import temp_hum_sense as ths
import luminance_sense
import paho.mqtt.client as mqtt
import sensor_msg
import time
import cooling
import config

class SimulatedStorageUnit:

    def __init__(self, storageUnitId, client, initialTemperature, initialHumidity, initialLuminance, initialPowerConsumption):
        self.storageUnitId = storageUnitId
        self.th_sensor = ths.DHT(storageUnitId, initialTemperature, 2, initialHumidity, 5)
        self.lux_sensor = luminance_sense.LightSensor(storageUnitId, initialLuminance, 2.0)
        self.cool_actuator = cooling.Cooling(self.storageUnitId, initialPowerConsumption, 15.0, client, self.th_sensor)
        self.client = client

        client.message_callback_add('cooling_state/' + self.storageUnitId , self.cool_actuator.on_message)
        client.message_callback_add('light_state/' + self.storageUnitId , self.lux_sensor.on_message)


    def startMeasure(self):
        print("===STARTING SIMULATED STORAGE UNIT " + self.storageUnitId + "===")
        topic = config.topic
        while True:
            temperatur_msg = sensor_msg.SensorMSG(self.th_sensor.temp_instance_id, self.th_sensor.temp_sensor_type, self.th_sensor.temp_unit, self.th_sensor.get_temp())
            self.client.publish(topic, temperatur_msg.to_json())
            time.sleep(1)
            humidity_msg = sensor_msg.SensorMSG(self.th_sensor.hum_instance_id, self.th_sensor.hum_sensor_type, self.th_sensor.hum_unit, self.th_sensor.get_hum())
            self.client.publish(topic, humidity_msg.to_json())
            time.sleep(1)
            lux_msg = sensor_msg.SensorMSG(self.lux_sensor.instanceId, self.lux_sensor.sensorType, self.lux_sensor.unit, self.lux_sensor.getLux())
            self.client.publish(topic, lux_msg.to_json())
            time.sleep(1)
            plug_power_msg = sensor_msg.SensorMSG(self.cool_actuator.instanceId + '_consumption', "power_consumption","W", self.cool_actuator.getPower())
            self.client.publish(topic, plug_power_msg.to_json())
            time.sleep(1)
            plug_state_msg = sensor_msg.SensorMSG(self.cool_actuator.instanceId + '_plug', "plug_state","state", self.cool_actuator.getPlugState())
            self.client.publish(topic, plug_state_msg.to_json())
            time.sleep(5)
