package de.uni_stuttgart.iaas.swaas.service.services;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Service
public class MqttService {

    static String publisherId = UUID.randomUUID().toString();
    @Value("${de.uni_stuttgart.iaas.swaas.broker.url}")
    private String serverURI;
    @Value("${de.uni_stuttgart.iaas.swaas.broker.user}")
    private String mqttUser;
    @Value("${de.uni_stuttgart.iaas.swaas.broker.pw}")
    private String mqttPassword;
    private IMqttClient publisher;
    private MemoryPersistence persistence = new MemoryPersistence();


    @PostConstruct
    public void init(){
        try {
            publisher = new MqttClient(serverURI,publisherId);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setUserName(mqttUser);
            connOpts.setPassword(mqttPassword.toCharArray());
            System.out.println("Connecting to broker: " + publisher.getServerURI());
            publisher.connect(connOpts);
            System.out.println("Connected");
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public IMqttClient getBroker(){
        return publisher;
    }

}
