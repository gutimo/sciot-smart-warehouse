export class SensorValue {
  value: number;
  unit: string;
}
