import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../model/product';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor() { }

  @Input()
  tableData: Product[];
  displayedColumns = ['name', 'customer', 'storedFrom', 'storedUntil', 'quantity', 'unit'];

  ngOnInit(): void {
  }

}
