import {StorageConditionBoundaries} from './storage-condition-boundaries';

export class StorageUnit {

  id: number;
  sensorIds: string[];
  productIds: number[];
  storageConditionsBoundaries: StorageConditionBoundaries;
}
