package de.uni_stuttgart.iaas.swaas.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StorageUnit {

    @Id
    private String id;

    @ElementCollection
    private List<String> sensorIds;

    @ElementCollection
    private List<Long> productIds;

    private StorageConditionsBoundaries storageConditionsBoundaries;
}
