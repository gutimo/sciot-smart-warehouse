import sensor_msg
import json
import random

class Cooling:

   

    def __init__(self, storageUnitId, initialPowerConsumption, powerConsumptionVariation, mqttclient, temp_hum):
        self.storageUnitId = storageUnitId
        self.instanceId = "simulated-" + storageUnitId + "-4"
        self.powerConsumption = initialPowerConsumption
        self.powerVariation = powerConsumptionVariation
        self.client = mqttclient
        self.plug_state = 1
        self.temp_hum = temp_hum



    
    def getPower(self):
        power = self.powerConsumption - self.powerVariation / 2 + random.random() * self.powerVariation
        if (power < 0):
            return 0
        else:
            return power
    
    def getPlugState(self):
        return self.plug_state


    def on_message(self, client, userdata, msg):
     # The callback for when a PUBLISH message is received from the server.
        message = msg.payload.decode('cp1252')
        plug = -1
        if(message == 'ON'):
            plug = 1
            self.temp_hum.decrease()
            print("Cooling ON in storage unit: " + self.storageUnitId)
        else:
            plug = 0
            self.temp_hum.increase()
            print("Cooling OFF in storage unit: " + self.storageUnitId)
        
        self.plug_state = plug


        
