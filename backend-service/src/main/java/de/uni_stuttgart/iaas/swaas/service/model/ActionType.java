package de.uni_stuttgart.iaas.swaas.service.model;

public enum ActionType {
    STORE, UNSTORE
}
