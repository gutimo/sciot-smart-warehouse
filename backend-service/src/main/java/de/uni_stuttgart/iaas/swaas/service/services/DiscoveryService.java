package de.uni_stuttgart.iaas.swaas.service.services;

import de.uni_stuttgart.iaas.swaas.service.model.*;
import de.uni_stuttgart.iaas.swaas.service.repos.ProductItemRepository;
import de.uni_stuttgart.iaas.swaas.service.repos.SensorRepository;
import de.uni_stuttgart.iaas.swaas.service.repos.SensorTypeRepository;
import de.uni_stuttgart.iaas.swaas.service.repos.StorageUnitRepository;
import de.uni_stuttgart.iaas.swaas.service.util.InfluxUtils;
import lombok.extern.java.Log;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log
@Service
public class DiscoveryService {

    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private SensorTypeRepository sensorTypeRepository;

    @Autowired
    private ProductItemRepository productItemRepository;

    @Autowired
    private StorageUnitRepository storageUnitRepository;

    @Autowired
    private StorageUnitsService storageUnitsService;

    private boolean firstDiscoveryFinished = false;

    @Scheduled(fixedRateString = "5000")
    public void discoverSensors() {
        Map<String, String> sensorMapping = new HashMap<>();
        sensorMapping.put("0x04cf8cdf3c7b23a0_plug", "1001");
        sensorMapping.put("0x04cf8cdf3c7b23a0_consumption", "1001");
        sensorMapping.put("202481592543589-1", "1001");
        sensorMapping.put("202481592543589-2", "1001");
        sensorMapping.put("202481592543589-3", "1001");
        sensorMapping.put("simulated-1002-1", "1002");
        sensorMapping.put("simulated-1002-2", "1002");
        sensorMapping.put("simulated-1002-3", "1002");
        sensorMapping.put("simulated-1002-4_plug", "1002");
        sensorMapping.put("simulated-1002-4_consumption", "1002");
        sensorMapping.put("simulated-1003-1", "1003");
        sensorMapping.put("simulated-1003-2", "1003");
        sensorMapping.put("simulated-1003-3", "1003");
        sensorMapping.put("simulated-1003-4_plug", "1003");
        sensorMapping.put("simulated-1003-4_consumption", "1003");

        QueryResult sensors = InfluxUtils.getSensorDatabase().query(new Query("SELECT * FROM /.*/ GROUP BY *", "sensors"));
        List<QueryResult.Series> series = sensors.getResults().get(0).getSeries();
        series.forEach(series1 -> {
            if (!sensorRepository.existsById(series1.getTags().get("instanceId")))
                sensorRepository.save(new Sensor(series1.getTags().get("instanceId"), series1.getName(), sensorMapping.get(series1.getTags().get("instanceId"))));
            if(!sensorTypeRepository.existsById(series1.getTags().get("instanceId")))
                sensorTypeRepository.save(new SensorType(series1.getName(), series1.getName().substring(series1.getName().lastIndexOf("/") + 1)));
        });
        if(!firstDiscoveryFinished) {
            initDemoWarehouse();
            firstDiscoveryFinished = true;
        }
    }

    public void initDemoWarehouse() {

        // Init storage unit 1003
        List<Sensor> sensors = sensorRepository.findByStorageUnit("1003");
        StorageUnit storageUnit = new StorageUnit();
        storageUnit.setId("1003");
        List<String> sensorIds = new ArrayList<>();
        for (Sensor sensor : sensors) {
            sensorIds.add(sensor.getInstanceId());
        }
        storageUnit.setSensorIds(sensorIds);
        List<Long> productIds = new ArrayList<>();

        ProductItem item = new ProductItem();
        item.setName("Icey Icecream");
        item.setCustomer("Igor Ices GmbH");
        item.setStoredFrom(Instant.now().toEpochMilli() - 1000 * 60 * 60 * 24 * 3);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 10);
        item.setTemperatureMin(-15.0d);
        item.setTemperatureMax(-3.0d);
        item.setHumidityMin(10.0d);
        item.setHumidityMax(80.0d);
        item.setLightMin(0.0d);
        item.setLightMax(5.0d);
        item.setQuantity(15.0d);
        item.setUnit("gram");
        item.setStorageUnitId("1003");
        ProductItem saved = productItemRepository.save(item);
        productIds.add(saved.getId());

        item = new ProductItem();
        item.setName("Cocktail Ice");
        item.setCustomer("Cool Drinks GmbH");
        item.setStoredFrom(Instant.now().toEpochMilli() - 1000 * 60 * 60 * 24 * 5);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 180);
        item.setTemperatureMin(-20.0d);
        item.setTemperatureMax(-12.0d);
        item.setHumidityMin(10.0d);
        item.setHumidityMax(80.0d);
        item.setLightMin(0.0d);
        item.setLightMax(5.0d);
        item.setQuantity(100.0d);
        item.setUnit("gram");
        item.setStorageUnitId("1003");
        saved = productItemRepository.save(item);
        productIds.add(saved.getId());

        item = new ProductItem();
        item.setName("Expensive IceCream");
        item.setCustomer("Expensive IceCreams AG");
        item.setStoredFrom(Instant.now().toEpochMilli() - 1000 * 60 * 60 * 24 * 2);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 6);
        item.setTemperatureMin(-15.0d);
        item.setTemperatureMax(-5.0d);
        item.setHumidityMin(10.0d);
        item.setHumidityMax(80.0d);
        item.setLightMin(0.0d);
        item.setLightMax(5.0d);
        item.setQuantity(300.0d);
        item.setUnit("gram");
        item.setStorageUnitId("1003");
        productItemRepository.save(item);
        productIds.add(saved.getId());

        storageUnit.setProductIds(productIds);
        storageUnit.setStorageConditionsBoundaries(new StorageConditionsBoundaries());
        storageUnitRepository.save(storageUnit);
        storageUnitsService.updateStorageConditionsBoundariesForStorageUnit("1003");

        // Init storage unit 1001
        sensors = sensorRepository.findByStorageUnit("1001");
        storageUnit = new StorageUnit();
        storageUnit.setId("1001");
        sensorIds = new ArrayList<>();
        for (Sensor sensor : sensors) {
            sensorIds.add(sensor.getInstanceId());
        }
        storageUnit.setSensorIds(sensorIds);
        productIds = new ArrayList<>();

        item = new ProductItem();
        item.setName("Pinot Blanc");
        item.setCustomer("Pasta, Pizza et Vino GmbH");
        item.setStoredFrom(Instant.now().toEpochMilli() - 1000 * 60 * 60 * 24 * 4);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 9);
        item.setTemperatureMin(9.0d);
        item.setTemperatureMax(22.0d);
        item.setHumidityMin(10.0d);
        item.setHumidityMax(90.0d);
        item.setLightMin(0.0d);
        item.setLightMax(5.0d);
        item.setQuantity(1.0d);
        item.setUnit("liter");
        item.setStorageUnitId("1001");
        saved = productItemRepository.save(item);
        productIds.add(saved.getId());

        storageUnit.setProductIds(productIds);
        storageUnit.setStorageConditionsBoundaries(new StorageConditionsBoundaries());
        storageUnitRepository.save(storageUnit);
        storageUnitsService.updateStorageConditionsBoundariesForStorageUnit("1001");


        // Init storage unit 1002
        sensors = sensorRepository.findByStorageUnit("1002");
        storageUnit = new StorageUnit();
        storageUnit.setId("1002");
        sensorIds = new ArrayList<>();
        for (Sensor sensor : sensors) {
            sensorIds.add(sensor.getInstanceId());
        }
        storageUnit.setSensorIds(sensorIds);
        productIds = new ArrayList<>();

        item = new ProductItem();
        item.setName("Potting soil");
        item.setCustomer("Flowers GmbH & CO.KG");
        item.setStoredFrom(Instant.now().toEpochMilli() - 1000 * 60 * 60 * 24 * 2);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 4);
        item.setTemperatureMin(15.0d);
        item.setTemperatureMax(25.0d);
        item.setHumidityMin(70.0d);
        item.setHumidityMax(100.0d);
        item.setLightMin(0.0d);
        item.setLightMax(650.0d);
        item.setQuantity(23.0d);
        item.setUnit("kg");
        item.setStorageUnitId("1002");
        saved = productItemRepository.save(item);
        productIds.add(saved.getId());

        storageUnit.setProductIds(productIds);
        storageUnit.setStorageConditionsBoundaries(new StorageConditionsBoundaries());
        storageUnitRepository.save(storageUnit);
        storageUnitsService.updateStorageConditionsBoundariesForStorageUnit("1002");


        // To be assigned to 1001
        item = new ProductItem();
        item.setName("Cool Cerveza");
        item.setCustomer("Cool Brewery Ltd.");
        item.setStoredFrom(Instant.now().toEpochMilli() + 1000 * 120);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 12);
        item.setTemperatureMin(7);
        item.setTemperatureMax(13.0d);
        item.setHumidityMin(10.0d);
        item.setHumidityMax(90.0d);
        item.setLightMin(0.0d);
        item.setLightMax(5.0d);
        item.setQuantity(1.0d);
        item.setUnit("liter");
        productItemRepository.save(item);

        // To be assigned to 1002
        item = new ProductItem();
        item.setName("Hortensie");
        item.setCustomer("Flowers GmbH & CO.KG");
        item.setStoredFrom(Instant.now().toEpochMilli() + 1000 * 240);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 4);
        item.setTemperatureMin(15.0d);
        item.setTemperatureMax(20.0d);
        item.setHumidityMin(70.0d);
        item.setHumidityMax(100.0d);
        item.setLightMin(500.0d);
        item.setLightMax(600.0d);
        item.setQuantity(7.0d);
        item.setUnit("piece");
        productItemRepository.save(item);

        // To be assigned to 1003
        item = new ProductItem();
        item.setName("Cheap IceCream");
        item.setCustomer("Cheap IceCreams AG");
        item.setStoredFrom(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 2);
        item.setStoredUntil(Instant.now().toEpochMilli() + 1000 * 60 * 60 * 24 * 12);
        item.setTemperatureMin(-14.0d);
        item.setTemperatureMax(-6.0d);
        item.setHumidityMin(10.0d);
        item.setHumidityMax(80.0d);
        item.setLightMin(0.0d);
        item.setLightMax(5.0d);
        item.setQuantity(200.0d);
        item.setUnit("gram");
        productItemRepository.save(item);
    }
}
