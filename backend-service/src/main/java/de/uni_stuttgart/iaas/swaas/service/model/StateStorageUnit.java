package de.uni_stuttgart.iaas.swaas.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StateStorageUnit {
    private List<ProductItem> storedProductItems;
    private StorageConditionsBoundaries storageConditionsBoundaries;
}
