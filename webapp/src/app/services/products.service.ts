import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Product} from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  url = 'api/products';
  public products: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);

  public getProducts(unitId: number): Observable<Product[]> {
    return this.http.get<Product[]>(this.url + '/forUnit/' + unitId)
      .pipe(
        map(rawProducts => rawProducts.map( rawProduct => this.transformRawProduct(rawProduct)))
      );
  }

  public getAllProducts(): void {
    this.http.get<Product[]>(this.url + '/all')
      .pipe(
        map(rawProducts => rawProducts.map( rawProduct => this.transformRawProduct(rawProduct)))
      )
      .subscribe(products => {
        this.products.next(products);
      });
  }

  public getStorageAssignment(): void {
    this.http.get<Product[]>(this.url + '/storageAssignment')
      .pipe(
        map(rawProducts => rawProducts.map( rawProduct => this.transformRawProduct(rawProduct)))
      )
      .subscribe(products => {
        this.products.next(products);
      });
  }

  transformRawProduct = (rawProduct) => {
    return {
      ...rawProduct,
      storedFrom: new Date(rawProduct.storedFrom),
      storedUntil: new Date(rawProduct.storedUntil)
    } as Product;
  }

  isProductToBeStored = (product: Product) => new Date().getTime() < product.storedFrom.getTime();
  isProductStored = (product: Product) => product.storageUnitId !== undefined && product.storedFrom.getTime() <= new Date().getTime() && new Date().getTime() <= product.storedUntil.getTime();
  isProductFinished = (product: Product) => product.storedUntil.getTime() < new Date().getTime();
}
