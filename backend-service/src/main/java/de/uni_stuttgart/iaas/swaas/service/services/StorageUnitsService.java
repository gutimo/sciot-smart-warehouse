package de.uni_stuttgart.iaas.swaas.service.services;

import de.uni_stuttgart.iaas.swaas.service.model.ProductItem;
import de.uni_stuttgart.iaas.swaas.service.model.StorageConditionsBoundaries;
import de.uni_stuttgart.iaas.swaas.service.model.StorageUnit;
import de.uni_stuttgart.iaas.swaas.service.repos.StorageUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StorageUnitsService {

    @Autowired
    private StorageUnitRepository storageUnitRepository;

    @Autowired
    private ProductsService productsService;

    public List<StorageUnit> getAllStorageUnits(){
        return storageUnitRepository.findAll();
    }

    public StorageUnit getStorageUnitById(String id){
        return storageUnitRepository.findById(id).get();
    }

    public void updateStorageConditionsBoundariesForStorageUnit(String storageUnitId) {
        List<ProductItem> productItems = productsService.getStoredProductsForUnit(storageUnitId);
        StorageConditionsBoundaries storageConditionsBoundaries = new StorageConditionsBoundaries();
        if (productItems.size() > 0) {
            storageConditionsBoundaries.setHumidityMin(productItems.get(0).getHumidityMin());
            storageConditionsBoundaries.setHumidityMax(productItems.get(0).getHumidityMax());
            storageConditionsBoundaries.setTemperatureMin(productItems.get(0).getTemperatureMin());
            storageConditionsBoundaries.setTemperatureMax(productItems.get(0).getTemperatureMax());
            storageConditionsBoundaries.setLightMin(productItems.get(0).getLightMin());
            storageConditionsBoundaries.setLightMax(productItems.get(0).getLightMax());
            productItems.forEach(productItem -> {
                storageConditionsBoundaries.setHumidityMin(Math.max(productItem.getHumidityMin(), storageConditionsBoundaries.getHumidityMin()));
                storageConditionsBoundaries.setHumidityMax(Math.min(productItem.getHumidityMax(), storageConditionsBoundaries.getHumidityMax()));
                storageConditionsBoundaries.setTemperatureMin(Math.max(productItem.getTemperatureMin(), storageConditionsBoundaries.getTemperatureMin()));
                storageConditionsBoundaries.setTemperatureMax(Math.min(productItem.getTemperatureMax(), storageConditionsBoundaries.getTemperatureMax()));
                storageConditionsBoundaries.setLightMin(Math.max(productItem.getLightMin(), storageConditionsBoundaries.getLightMin()));
                storageConditionsBoundaries.setLightMax(Math.min(productItem.getLightMax(), storageConditionsBoundaries.getLightMax()));
            });
        }

        StorageUnit storageUnit = getStorageUnitById(storageUnitId);
        storageUnit.setStorageConditionsBoundaries(storageConditionsBoundaries);
        storageUnitRepository.save(storageUnit);
    }



}
