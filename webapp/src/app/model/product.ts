export class Product {
  id: number;
  name: string;
  customer: string;
  storageUnitId: string;
  storedFrom: Date;
  storedUntil: Date;
  quantity: number;
  unit: string;
  humidityMin: number;
  temperatureMin: number;
  lightMin: number;
  humidityMax: number;
  temperatureMax: number;
  lightMax: number;
}
