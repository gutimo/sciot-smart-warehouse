import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SensorPoint} from '../model/sensor-point';

@Injectable({
  providedIn: 'root'
})
export class SensorsService {

  url = 'api/sensors';

  constructor(private http: HttpClient) { }

  public getSensor(sensorId: string, days: number): Observable<SensorPoint[]> {
    return this.http.get<SensorPoint[]>(this.url + '/get/sensorId=' + sensorId + ',days=' + days);
  }

  public getSensorSeconds(sensorId: string, seconds: number): Observable<SensorPoint[]> {
    return this.http.get<SensorPoint[]>(this.url + '/get/sensorId=' + sensorId + ',seconds=' + seconds);
  }

}
