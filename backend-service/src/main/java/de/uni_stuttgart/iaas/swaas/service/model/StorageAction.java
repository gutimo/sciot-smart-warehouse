package de.uni_stuttgart.iaas.swaas.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StorageAction {
    private ActionType actionType;
    private long productId;
    private String storageUnitId;
    private long scheduledTime;
}


