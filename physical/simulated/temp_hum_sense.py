import random

class DHT:
    temp_sensor_type = "temperature"
    hum_sensor_type = "humidity"

    temp_unit = "°C"
    hum_unit = "%"

    def __init__(self, storageUnitId, initialTemperature, temperatureVariation, initialHumidity, humidityVariation):
        self.storageUnitId = storageUnitId
        self.temp_instance_id = "simulated-" + storageUnitId + "-1"
        self.hum_instance_id = "simulated-" + storageUnitId + "-2"
        self.temperature = initialTemperature
        self.temperatureVariation = temperatureVariation
        self.humidity = initialHumidity
        self.humidityVariation = humidityVariation

    def get_temp(self):
        rand = round(random.random(), 3)
        return self.temperature - self.temperatureVariation / 2 + rand * self.temperatureVariation

    def get_hum(self):
        humidity = self.humidity - self.humidityVariation / 2 + random.random() * self.temperatureVariation
        if (humidity < 0):
            return 0
        else:
            return humidity

    def increase(self):
        self.temperature += 0.5

    def decrease(self):
        self.temperature -= 0.5

    def on_message(self, client, userdata, msg):
        # The callback for when a PUBLISH message is received from the server.
        message = msg.payload.decode('cp1252')
        if(message == 'ON'):
            self.temperature -= 0.5
        else:
            print(message)
            self.temperature += 0.5
