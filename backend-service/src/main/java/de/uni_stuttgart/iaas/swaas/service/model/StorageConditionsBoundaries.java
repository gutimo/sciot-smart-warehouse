package de.uni_stuttgart.iaas.swaas.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StorageConditionsBoundaries {
    private double humidityMin;
    private double temperatureMin;
    private double lightMin;
    private double humidityMax;
    private double temperatureMax;
    private double lightMax;
}
