package de.uni_stuttgart.iaas.swaas.service.services;

import de.uni_stuttgart.iaas.swaas.service.model.ProductItem;
import de.uni_stuttgart.iaas.swaas.service.model.StorageAction;
import de.uni_stuttgart.iaas.swaas.service.model.StorageUnit;
import de.uni_stuttgart.iaas.swaas.service.repos.ProductItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Service
public class ProductsService {

    @Autowired
    private ProductItemRepository productItemRepository;

    @Autowired
    private StorageUnitsService storageUnitsService;

    @Autowired
    private PlannerService plannerService;

    public Iterable<ProductItem> getAllProducts() {
        return productItemRepository.findAll();
    }

    public List<ProductItem> getProductsForUnit(String storageUnitId) {
        return productItemRepository.findAllByStorageUnitId(storageUnitId);
    }

    public ProductItem getProductById(long productId) {
        return productItemRepository.findById(productId);
    }

    public List<ProductItem> getIncomingProducts() {
        return productItemRepository.findAllByStoredFromAfter(Instant.now().toEpochMilli());
    }

    public List<ProductItem> getStoredProducts() {
        return productItemRepository.findAllByStoredFromBeforeAndStoredUntilAfter(Instant.now().toEpochMilli(), Instant.now().toEpochMilli());
    }

    public List<ProductItem> getStoredProductsForUnit(String storageUnitId) {
        return productItemRepository.findAllByStorageUnitIdAndStoredFromBeforeAndStoredUntilAfter(storageUnitId, Instant.now().toEpochMilli(), Instant.now().toEpochMilli());
    }

    public ProductItem saveProduct(ProductItem productItem) {
        return productItemRepository.save(productItem);
    }
}
