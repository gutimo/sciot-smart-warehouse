export class SensorPoint {

  measure: string;
  instanceId: string;
  unit: string;
  value: number;
  timestamp: Date;

}
