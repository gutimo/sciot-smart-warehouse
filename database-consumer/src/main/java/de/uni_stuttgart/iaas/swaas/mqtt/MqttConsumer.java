package de.uni_stuttgart.iaas.swaas.mqtt;

import de.uni_stuttgart.iaas.swaas.config.Configuration;
import de.uni_stuttgart.iaas.swaas.db.InfluxDBAdapter;
import lombok.SneakyThrows;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import static de.uni_stuttgart.iaas.swaas.config.Configuration.*;

public class MqttConsumer implements Runnable {

    String topic = getTopicUrl();
    String broker = getBrokerUrl();
    String clientId = "DATABASESERVICE";
    MemoryPersistence persistence = new MemoryPersistence();

    boolean interrupted = false;


    @SneakyThrows
    @Override
    public void run() {
        init();
        while (!interrupted) {
            Thread.sleep(1000);
        }
    }

    public void init() {
        try {
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setUserName(getBrokerUser());
            connOpts.setPassword(getBrokerPassword().toCharArray());
            System.out.println("Connecting to broker: " + broker);
            sampleClient.connect(connOpts);
            System.out.println("Connected");

            sampleClient.subscribe(topic, (topicname, msg) -> {
                System.out.println(msg.toString());
                InfluxDBAdapter.getInstance().insertPoint(msg.toString());
            });

        } catch (MqttSecurityException e) {
            e.printStackTrace();
        } catch (MqttPersistenceException e) {
            e.printStackTrace();
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }

    public void stop() {
        this.interrupted = true;
    }
}
