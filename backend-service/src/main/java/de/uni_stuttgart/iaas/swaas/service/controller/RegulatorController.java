package de.uni_stuttgart.iaas.swaas.service.controller;

import de.uni_stuttgart.iaas.swaas.service.model.Action;
import de.uni_stuttgart.iaas.swaas.service.services.RegulatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@RestController
public class RegulatorController {

    private static final String context = "/regulator";

    @Autowired
    private RegulatorService regulatorService;

    @PostMapping(context + "/execute/single/{unitId}")
    public ResponseEntity<Boolean> execAction(@PathVariable String unitId, @RequestBody Action action) {
        return new ResponseEntity<>(regulatorService.execAction(unitId, action), HttpStatus.OK);
    }

    @PostMapping(context + "/execute/list/{unitId}")
    public ResponseEntity<Boolean> execActions(@PathVariable String unitId, @RequestBody List<Action> actions) {
        return new ResponseEntity<>(regulatorService.execActions(unitId, actions), HttpStatus.OK);
    }


}
