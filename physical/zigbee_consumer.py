import paho.mqtt.client as mqtt
import sensor_msg
import json

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT broker with result code "+str(rc))

    client.subscribe("zigbee2mqtt/0x04cf8cdf3c7b23a0")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg))
    try:
        message = msg.payload.decode('cp1252')
        unpacked_json = json.loads(message)
    except Exception as e:
        print("Couldn't parse raw data: %s" % msg.payload, e)
    else:
        plug_power_msg = sensor_msg.SensorMSG("0x04cf8cdf3c7b23a0","power_consumption","W", unpacked_json["power"])
        client.publish("grovepi", plug_power_msg.to_json())
        plug = -1
        if(unpacked_json["state"] == 'ON'):
            plug = 1
        else:
            plug = 0

        plug_state_msg = sensor_msg.SensorMSG("0x04cf8cdf3c7b23a0","plug_state","state", plug)
        client.publish("grovepi", plug_state_msg.to_json())

def start():
    print("===STARTUP===")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    print("Trying to connect to broker")
    # TODO Extract credatials and host config
    client.username_pw_set("grovepi", password="grovepi")
    client.connect("gutierrez.one", 1883)
    client.loop_forever()
