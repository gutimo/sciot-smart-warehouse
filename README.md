# SCIoT - Smart Warehouse
This is the repo for an student project implementing an example of an smart building.
In particular this project is concerned with an smwart warehouse management system.

## backend-service
Contains a Spring-Boot Java application it contains:
* REST API
* Planner for product assignemnt
* Regulator of Storage-Units
* Websocket for updating sensor values 

## databse-consumer
Contains:
* a consumer that writes sensor values into an influxDB

## physical
Contains:
* scripts for accessing sensors and acutators of a raspberry pi
* scripts for simulating said sesnors and actuators

## webapp
An angular app with views for :
* monitoring storage units
* assigning products
