import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageUnit} from '../model/storage-unit';


// @ts-ignore
@Injectable({
  providedIn: 'root'
})
export class StorageUnitsService {

  url = 'api/units';

  constructor(private http: HttpClient) { }

  public getAllUnits(): Observable<StorageUnit[]> {
    return this.http.get<StorageUnit[]>(this.url + '/all');
  }

}


