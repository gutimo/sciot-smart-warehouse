package de.uni_stuttgart.iaas.swaas.service.controller;

import de.uni_stuttgart.iaas.swaas.service.model.Sensor;
import de.uni_stuttgart.iaas.swaas.service.model.SensorType;
import de.uni_stuttgart.iaas.swaas.service.model.influx.SensorPoint;
import de.uni_stuttgart.iaas.swaas.service.services.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class SensorController {

    private static final String context = "/sensors";


    @Autowired
    private SensorService sensorService;

    @GetMapping(context + "/all")
    public ResponseEntity<Iterable<Sensor>> getSensors(){
        return new ResponseEntity<>(sensorService.getSensors(), HttpStatus.OK);
    }

    @GetMapping(context +"/types")
    public ResponseEntity<Iterable<SensorType>> getSensorTypes() {
        return new ResponseEntity<>(sensorService.getSensorTypes(), HttpStatus.OK);
    }


    @GetMapping(context + "/get/sensorId={sensorId},days={days}")
    public ResponseEntity<Iterable<SensorPoint>> getSensors(@PathVariable String sensorId, @PathVariable int days){
        return new ResponseEntity<>(sensorService.getSensorData(sensorId, days), HttpStatus.OK);
    }

    @GetMapping(context + "/get/sensorId={sensorId},seconds={seconds}")
    public ResponseEntity<Iterable<SensorPoint>> getSensorsSeconds(@PathVariable String sensorId, @PathVariable int seconds){
        return new ResponseEntity<>(sensorService.getSensorDataSeconds(sensorId, seconds), HttpStatus.OK);
    }



}
