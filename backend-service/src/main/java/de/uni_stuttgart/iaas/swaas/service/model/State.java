package de.uni_stuttgart.iaas.swaas.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class State {
    public State(State state) {
        State newState = new State();
        newState.setStateStorageUnits(new HashMap<>());
        for (String stateStorageUnitId : state.getStateStorageUnits().keySet()) {
            StateStorageUnit newStateStorageUnit = new StateStorageUnit();

            StorageConditionsBoundaries oldStorageConditionsBoundaries = state.getStateStorageUnits().get(stateStorageUnitId).getStorageConditionsBoundaries();
            StorageConditionsBoundaries newStorageConditionsBoundaries = new StorageConditionsBoundaries();
            newStorageConditionsBoundaries.setHumidityMax(oldStorageConditionsBoundaries.getHumidityMax());
            newStorageConditionsBoundaries.setHumidityMin(oldStorageConditionsBoundaries.getHumidityMin());
            newStorageConditionsBoundaries.setLightMax(oldStorageConditionsBoundaries.getLightMax());
            newStorageConditionsBoundaries.setLightMin(oldStorageConditionsBoundaries.getLightMin());
            newStorageConditionsBoundaries.setTemperatureMax(oldStorageConditionsBoundaries.getTemperatureMax());
            newStorageConditionsBoundaries.setTemperatureMax(oldStorageConditionsBoundaries.getTemperatureMax());

            List<ProductItem> oldStoredProductItems = state.getStateStorageUnits().get(stateStorageUnitId).getStoredProductItems();
            List<ProductItem> newStoredProductItems = new ArrayList<>();
            for (ProductItem oldProductItem : oldStoredProductItems) {
                ProductItem newProductItem = new ProductItem();
                newProductItem.setId(oldProductItem.getId());
                newProductItem.setName(oldProductItem.getName());
                newProductItem.setCustomer(oldProductItem.getCustomer());
                newProductItem.setStorageUnitId(oldProductItem.getStorageUnitId());
                newProductItem.setStoredFrom(oldProductItem.getStoredFrom());
                newProductItem.setStoredUntil(oldProductItem.getStoredUntil());
                newProductItem.setQuantity(oldProductItem.getQuantity());
                newProductItem.setUnit(oldProductItem.getUnit());

                newProductItem.setHumidityMin(oldProductItem.getHumidityMin());
                newProductItem.setHumidityMax(oldProductItem.getHumidityMax());
                newProductItem.setTemperatureMin(oldProductItem.getTemperatureMin());
                newProductItem.setTemperatureMax(oldProductItem.getTemperatureMax());
                newProductItem.setLightMin(oldProductItem.getLightMin());
                newProductItem.setLightMax(oldProductItem.getLightMax());
                newStoredProductItems.add(newProductItem);
            }

            newStateStorageUnit.setStorageConditionsBoundaries(newStorageConditionsBoundaries);
            newStateStorageUnit.setStoredProductItems(newStoredProductItems);
            newState.getStateStorageUnits().put(stateStorageUnitId, newStateStorageUnit);
        }
        this.stateStorageUnits = newState.getStateStorageUnits();
    }
    private Map<String, StateStorageUnit> stateStorageUnits;
}
