import paho.mqtt.client as mqtt
import sensor_msg
import json



def on_message(client, userdata, msg):
    # The callback for when a PUBLISH message is received from the server.
    message = msg.payload.decode('cp1252')
    client.publish("zigbee2mqtt/0x04cf8cdf3c7b23a0/set","{\"state\" : \""+ message +"\"}")
    client.publish("zigbee2mqtt/0x04cf8cdf3c7b23a0", "{\"state\" : \""+ message +"\"}")
