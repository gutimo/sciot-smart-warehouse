import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  constructor() { }

  @Input()
  multi: any[];
  view: any[] = [window.innerWidth / 4, 250];

  // options
  legend = false;
  showLabels = true;
  animations = true;
  xAxis = false;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = false;
  xAxisLabel = 'Time';
  @Input()
  yAxisLabel = 'humidity in %';
  timeline = true;

  @Input()
  primaryColor = '#5aa454';

  colorScheme;

  ngOnInit(): void {
    this.colorScheme = {
      domain: [this.primaryColor, '#fc5a03', '#fc5a03']
    };
  }


  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

}
