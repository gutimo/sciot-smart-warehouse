package de.uni_stuttgart.iaas.swaas.service.services;

import de.uni_stuttgart.iaas.swaas.service.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlannerService {
    @Autowired
    private StorageUnitsService storageUnitsService;

    @Autowired
    private ProductsService productsService;

    Timer daemonTimer;

    List<StorageAction> sortedStorageActions = new ArrayList<>();
    List<StorageAction> plannedStorageActions = new ArrayList<>();
    int sortedStorageActionsPointer = 0;

    public Iterable<ProductItem> updateStorageAssignment() {
        List<StorageAction> plan = plan();
        daemonTimer = new Timer("DaemonTimer", true);
        // set storageUnitId of product, set new products array of storageUnit and schedule store / unstore action
        plan.forEach(storageAction -> {
            ProductItem productItem = this.productsService.getProductById(storageAction.getProductId());
            productItem.setStorageUnitId(storageAction.getStorageUnitId());
            this.productsService.saveProduct(productItem);

            StorageUnit storageUnit = this.storageUnitsService.getStorageUnitById(storageAction.getStorageUnitId());
            storageUnit.getProductIds().add(storageAction.getProductId());
            daemonTimer.schedule(new ActionTimerTask(storageAction), Date.from(Instant.ofEpochMilli(storageAction.getScheduledTime()).plusSeconds(1)));
        });

        return this.productsService.getAllProducts();
    }

    // TODO: Add real AI-Planning
    public List<StorageAction> plan() {
        List<StorageUnit> storageUnits = storageUnitsService.getAllStorageUnits();
        List<ProductItem> incomingProductItems = productsService.getIncomingProducts();
        List<ProductItem> storedProductItems = productsService.getStoredProducts();

        List<StorageAction> storageActions = new ArrayList<>();

        // create startState containing storageUnits with currently stored products
        State startState = new State();
        startState.setStateStorageUnits(new HashMap<>());
        storageUnits.forEach(storageUnit -> {
            startState.getStateStorageUnits().put(storageUnit.getId(), new StateStorageUnit(productsService.getStoredProductsForUnit(storageUnit.getId()), new StorageConditionsBoundaries()));
        });

        // create list of storageActions that must be performed in this order, For ActionType.Store actions the assignment is not set yet and will be searched for recursively
        storedProductItems.addAll(incomingProductItems);
        incomingProductItems.sort(byFromDate);
        storedProductItems.sort(byUntilDate);
        this.sortedStorageActions = new ArrayList<>();
        int targetSize = incomingProductItems.size() + storedProductItems.size();
        while (this.sortedStorageActions.size() < targetSize) {
            if (incomingProductItems.size() == 0) {
                this.sortedStorageActions.add(new StorageAction(ActionType.UNSTORE, storedProductItems.get(0).getId(), storedProductItems.get(0).getStorageUnitId(), storedProductItems.get(0).getStoredUntil()));
                storedProductItems.remove(0);
                continue;
            } else if (storedProductItems.size() == 0) {
                this.sortedStorageActions.add(new StorageAction(ActionType.STORE, incomingProductItems.get(0).getId(), "", incomingProductItems.get(0).getStoredFrom()));
                incomingProductItems.remove(0);
                continue;
            }
            if (incomingProductItems.get(0).getStoredFrom() < storedProductItems.get(0).getStoredUntil()) {
                this.sortedStorageActions.add(new StorageAction(ActionType.STORE, incomingProductItems.get(0).getId(), "", incomingProductItems.get(0).getStoredFrom()));
                incomingProductItems.remove(0);
            } else {
                this.sortedStorageActions.add(new StorageAction(ActionType.UNSTORE, storedProductItems.get(0).getId(), storedProductItems.get(0).getStorageUnitId(), storedProductItems.get(0).getStoredUntil()));
                storedProductItems.remove(0);
            }
        }

        this.sortedStorageActionsPointer = 0;
        this.plannedStorageActions = new ArrayList<>();
        int isFeasible = planRecursively(startState);
        if (isFeasible != 1) {
            throw new RuntimeException("No feasible plan found");
        }

        return this.plannedStorageActions;

    }

    private int planRecursively(State state) {
        // plan can be executed
        if (sortedStorageActionsPointer == this.sortedStorageActions.size()) {
            return 1;
        }

        StorageAction nextAction = this.sortedStorageActions.get(this.sortedStorageActionsPointer);
        List<StorageAction> nextPossibleActions = new ArrayList<>();
        if (nextAction.getActionType() == ActionType.STORE) {
            nextPossibleActions = createStorageActions(state, nextAction);
        } else {
            nextPossibleActions.add(nextAction);
        }

        for (StorageAction nextPossibleAction : nextPossibleActions) {
            State nextState = calculateNextState(state, nextPossibleAction);
            if (nextPossibleAction.getActionType() == ActionType.UNSTORE || areStorageConditionsFeasible(nextState.getStateStorageUnits().get(nextPossibleAction.getStorageUnitId()).getStorageConditionsBoundaries())) {
                this.plannedStorageActions.add(nextPossibleAction);
                this.sortedStorageActionsPointer += 1;
                int isFeasible = planRecursively(nextState);
                if (isFeasible == 1) {
                    return 1;
                } else {
                    this.plannedStorageActions.remove(this.plannedStorageActions.size() - 1);
                    this.sortedStorageActionsPointer -= 1;
                }
            }
        }
        // plan cannot be executed as no action was creating a feasible state
        return -1;

    }

    private boolean areStorageConditionsFeasible(StorageConditionsBoundaries storageConditionsBoundaries) {
        if (storageConditionsBoundaries.getHumidityMin() >= storageConditionsBoundaries.getHumidityMax()) {
            return false;
        }
        if (storageConditionsBoundaries.getLightMin() >= storageConditionsBoundaries.getLightMax()) {
            return false;
        }
        return !(storageConditionsBoundaries.getTemperatureMin() >= storageConditionsBoundaries.getTemperatureMax());
    }

    private List<StorageAction> createStorageActions(State state, StorageAction storageAction) {
        List<StorageAction> nextActions = new ArrayList<>();
        for (String stateStorageUnitId : state.getStateStorageUnits().keySet()) {
            nextActions.add(new StorageAction(storageAction.getActionType(), storageAction.getProductId(), stateStorageUnitId, storageAction.getScheduledTime()));
        }
        return nextActions;
    }

    private State calculateNextState(State state, StorageAction storageAction) {
        ProductItem productItem = productsService.getProductById(storageAction.getProductId());
        State newState = new State(state);
        if (storageAction.getActionType() == ActionType.UNSTORE) {
            String storageUnitIdOfProductItem = "";
            Map<String, StateStorageUnit> storageUnits = newState.getStateStorageUnits();
            for (String storageUnitId : storageUnits.keySet()) {
                StateStorageUnit stateStorageUnit = storageUnits.get(storageUnitId);
                for (ProductItem storedProductItem : stateStorageUnit.getStoredProductItems()) {
                    if (storedProductItem.getId() == productItem.getId()) {
                        storageUnitIdOfProductItem = storageUnitId;
                        productItem = storedProductItem;
                        storageAction.setStorageUnitId(storageUnitIdOfProductItem);
                        break;
                    }
                }
            }
            newState.getStateStorageUnits().get(storageUnitIdOfProductItem).getStoredProductItems().remove(productItem);
        } else {
            newState.getStateStorageUnits().get(storageAction.getStorageUnitId()).getStoredProductItems().add(productItem);
        }
        StorageConditionsBoundaries storageConditionsBoundaries = calculateStorageConditionsBoundariesForStorageUnit(newState.getStateStorageUnits().get(storageAction.getStorageUnitId()));
        newState.getStateStorageUnits().get(storageAction.getStorageUnitId()).setStorageConditionsBoundaries(storageConditionsBoundaries);
        return newState;
    }

    private StorageConditionsBoundaries calculateStorageConditionsBoundariesForStorageUnit(StateStorageUnit stateStorageUnit) {
        StorageConditionsBoundaries storageConditionsBoundaries = new StorageConditionsBoundaries();
        if (stateStorageUnit.getStoredProductItems().size() > 0) {
            storageConditionsBoundaries.setHumidityMin(stateStorageUnit.getStoredProductItems().get(0).getHumidityMin());
            storageConditionsBoundaries.setHumidityMax(stateStorageUnit.getStoredProductItems().get(0).getHumidityMax());
            storageConditionsBoundaries.setTemperatureMin(stateStorageUnit.getStoredProductItems().get(0).getTemperatureMin());
            storageConditionsBoundaries.setTemperatureMax(stateStorageUnit.getStoredProductItems().get(0).getTemperatureMax());
            storageConditionsBoundaries.setLightMin(stateStorageUnit.getStoredProductItems().get(0).getLightMin());
            storageConditionsBoundaries.setLightMax(stateStorageUnit.getStoredProductItems().get(0).getLightMax());
            stateStorageUnit.getStoredProductItems().forEach(productItem -> {
                storageConditionsBoundaries.setHumidityMin(Math.max(productItem.getHumidityMin(), storageConditionsBoundaries.getHumidityMin()));
                storageConditionsBoundaries.setHumidityMax(Math.min(productItem.getHumidityMax(), storageConditionsBoundaries.getHumidityMax()));
                storageConditionsBoundaries.setTemperatureMin(Math.max(productItem.getTemperatureMin(), storageConditionsBoundaries.getTemperatureMin()));
                storageConditionsBoundaries.setTemperatureMax(Math.min(productItem.getTemperatureMax(), storageConditionsBoundaries.getTemperatureMax()));
                storageConditionsBoundaries.setLightMin(Math.max(productItem.getLightMin(), storageConditionsBoundaries.getLightMin()));
                storageConditionsBoundaries.setLightMax(Math.min(productItem.getLightMax(), storageConditionsBoundaries.getLightMax()));
            });
        }
        return storageConditionsBoundaries;
    }

    final Comparator<ProductItem> byFromDate = Comparator.comparingLong(ProductItem::getStoredFrom);
    final Comparator<ProductItem> byUntilDate = Comparator.comparingLong(ProductItem::getStoredUntil);


    class ActionTimerTask extends TimerTask {

        private final StorageAction storageAction;

        public ActionTimerTask(final StorageAction storageAction)
        {
            this.storageAction = storageAction;
        }
        public void run()
        {
            storageUnitsService.updateStorageConditionsBoundariesForStorageUnit(storageAction.getStorageUnitId());
            System.out.println("StorageConditionsBoundaries updated");
        }
    }

}
