import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import {decode} from 'punycode';

@Component({
  selector: 'app-mainframe',
  templateUrl: './mainframe.component.html',
  styleUrls: ['./mainframe.component.scss']
})
export class MainframeComponent implements OnInit {


  constructor(
    public router: Router,
    public dialog: MatDialog
  ) { }



  ngOnInit(): void {
  }


  navigate(route) {
    this.router.navigate([route]);
  }

}
