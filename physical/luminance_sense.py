from uuid import getnode as get_mac
import grovepi

class LightSensor:

    instanceId = str(get_mac()) + "-3"
    sensorType = "luminance"
    unit = "lux"

    VoutArray =  [ 0.0011498,  0.0033908,   0.011498, 0.041803,0.15199,     0.53367, 1.3689,   1.9068,  2.3]
    LuxArray =  [ 1.0108,     3.1201,  9.8051,   27.43,   69.545,   232.67,  645.11,   73.52,  1000]


    def getLux(self):
        MeasuredVout = grovepi.analogRead(0) * (3.0 / 1023.0)
        Luminance = self.FmultiMap(MeasuredVout, self.VoutArray, self.LuxArray, 9)
        return Luminance


    def FmultiMap(self,val, _in, _out,  size):
        if val <= _in[0]:
            return _out[0]
        if val >= _in[size-1]:
            return _out[size-1]

        pos = 1
        while val > _in[pos]:
            pos += 1

        if val == _in[pos]:
            return _out[pos]

        return (val - _in[pos-1]) * (_out[pos] - _out[pos-1]) / (_in[pos] - _in[pos-1]) + _out[pos-1]