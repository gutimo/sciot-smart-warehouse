import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {StorageUnitsService} from '../../services/storage-units.service';
import {StorageUnit} from '../../model/storage-unit';
import {MatTabChangeEvent} from '@angular/material/tabs';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  constructor(
    private ref: ChangeDetectorRef,
    private storageUnitsService: StorageUnitsService) { }


  rooms: StorageUnit[];

  ngOnInit(): void {
    this.storageUnitsService.getAllUnits().subscribe(result => {
      this.rooms = result.sort(this.sortNum);
    });
  }


  onLinkClick(event: MatTabChangeEvent) {
    this.ref.detectChanges();
  }



  sortNum(a: StorageUnit, b: StorageUnit) {
      return a.id - b.id;
  }
}
