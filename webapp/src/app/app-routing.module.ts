import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RoomsComponent} from './components/rooms/rooms.component';
import {ProductAssignmentComponent} from './components/product-assignment/product-assignment.component';


const routes: Routes = [
  { path: '', component: RoomsComponent },
  { path: 'ProductAssignment', component: ProductAssignmentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
