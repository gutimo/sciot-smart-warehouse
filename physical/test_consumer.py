import paho.mqtt.client as mqtt

# Only use for testing the messages

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT broker with result code "+str(rc))

    client.subscribe("grovepi")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

print("===STARTUP===")
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
print("Trying to connect to broker")
# TODO Extract credatials and host config
client.username_pw_set("grovepi", password="grovepi")
client.connect("gutierrez.one", 1883)
client.loop_forever()