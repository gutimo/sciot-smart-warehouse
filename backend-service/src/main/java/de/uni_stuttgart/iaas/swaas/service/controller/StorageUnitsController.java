package de.uni_stuttgart.iaas.swaas.service.controller;

import de.uni_stuttgart.iaas.swaas.service.model.StorageUnit;
import de.uni_stuttgart.iaas.swaas.service.services.StorageUnitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StorageUnitsController {

    private static final String context = "/units";

    @Autowired
    private StorageUnitsService storageUnitsService;

    @GetMapping(context + "/all")
    public ResponseEntity<Iterable<StorageUnit>> getAllUnits(){
        return new ResponseEntity<>(storageUnitsService.getAllStorageUnits(), HttpStatus.OK);
    }

}
