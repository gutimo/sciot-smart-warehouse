package de.uni_stuttgart.iaas.swaas.service.services;

import de.uni_stuttgart.iaas.swaas.service.model.Sensor;
import de.uni_stuttgart.iaas.swaas.service.model.SensorType;
import de.uni_stuttgart.iaas.swaas.service.model.influx.SensorPoint;
import de.uni_stuttgart.iaas.swaas.service.repos.SensorRepository;
import de.uni_stuttgart.iaas.swaas.service.repos.SensorTypeRepository;
import de.uni_stuttgart.iaas.swaas.service.util.InfluxUtils;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static de.uni_stuttgart.iaas.swaas.service.util.InfluxUtils.parseSensorSeries;

@Service
public class SensorService {

    @Autowired
    private SensorRepository sensorRepository;

    @Autowired
    private SensorTypeRepository sensorTypeRepository;


    public Iterable<Sensor> getSensors() {
        return sensorRepository.findAll();
    }

    public List<Sensor> getSensorsByStorageUnitId(String storageUnitId) {
        return sensorRepository.findByStorageUnit(storageUnitId);
    }

    public Sensor getSensor(String sensorId) {
        return sensorRepository.findById(sensorId).get();
    }

    public Iterable<SensorType> getSensorTypes() {
        return sensorTypeRepository.findAll();
    }

    public Iterable<SensorPoint> getSensorData(String sensorId, int days) {
        QueryResult sensors = InfluxUtils.getSensorDatabase().query(new Query("SELECT * FROM /.*/ where instanceId ='" + sensorId + "' and time > now() - "+ days +"d", "sensors"));
        List<QueryResult.Series> series = sensors.getResults().get(0).getSeries();
        return parseSensorSeries(series);
    }

    public List<SensorPoint> getSensorDataSeconds(String sensorId, int seconds) {
        QueryResult sensors = InfluxUtils.getSensorDatabase().query(new Query("SELECT * FROM /.*/ where instanceId ='" + sensorId+"' and time > now() - "+ seconds +"s", "sensors"));
        List<QueryResult.Series> series = sensors.getResults().get(0).getSeries();
        return parseSensorSeries(series);
    }



}
