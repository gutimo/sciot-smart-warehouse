import random

class LightSensor:
    sensorType = "luminance"
    unit = "lux"

    def __init__(self, storageUnitId, initialLuminance, luminanceVariation):
        self.storageUnitId = storageUnitId
        self.instanceId = "simulated-" + storageUnitId + "-3"
        self.luminance = initialLuminance
        self.luminanceVariation = luminanceVariation

    def getLux(self):
        luminance = self.luminance - self.luminanceVariation / 2 + random.random() * self.luminanceVariation
        if (luminance < 0):
            return 0
        else:
            return luminance

    def on_message(self, client, userdata, msg):
        # The callback for when a PUBLISH message is received from the server.
        message = msg.payload.decode('cp1252')
        if(message == 'ON'):
            print("Light ON in storage unit: " + self.storageUnitId)
            self.luminance = 530.0
        else:
            print("Light OFF in storage unit: " + self.storageUnitId)
            self.luminance = 1.0
