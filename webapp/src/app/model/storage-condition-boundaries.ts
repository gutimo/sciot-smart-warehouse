export class StorageConditionBoundaries {

  humidityMin: number;
  temperatureMin: number;
  lightMin: number;
  humidityMax: number;
  temperatureMax: number;
  lightMax: number;

}
