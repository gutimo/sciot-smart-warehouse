package de.uni_stuttgart.iaas.swaas.service.repos;

import de.uni_stuttgart.iaas.swaas.service.model.StorageUnit;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StorageUnitRepository extends CrudRepository<StorageUnit, String> {

    Optional<StorageUnit> findById(String id);

    List<StorageUnit> findAll();
}
