package de.uni_stuttgart.iaas.swaas.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Sensor  {

    @Id
    private String instanceId;
    private String sensorType;
    private String storageUnit;

}
