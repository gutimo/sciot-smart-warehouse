import grovepi
from uuid import getnode as get_mac

class DHT:
    temp_instance_id = str(get_mac()) + "-1"
    hum_instance_id = str(get_mac()) + "-2"

    temp_sensor_type = "temperature"
    hum_sensor_type = "humidity"

    temp_unit = "°C"
    hum_unit = "%"


    def read_values(self):
        return grovepi.dht(4,0)

    def get_temp(self):
        return self.read_values()[0]

    def get_hum(self):
        return self.read_values()[1]
